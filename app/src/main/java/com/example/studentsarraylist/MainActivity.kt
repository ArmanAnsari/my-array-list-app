package com.example.studentsarraylist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Spinner

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val studentsName = findViewById<Spinner>(R.id.studentsName_spinner)
        val studentsNameList = arrayOf("Arman","Ansar","Akash","Sudish","Guddu","Chanchal","Gollu")

        val studentsNameAdapter = ArrayAdapter(this, R.layout.spinner_item,studentsNameList)
        studentsName.adapter = studentsNameAdapter
        
        val listView = findViewById<ListView>(R.id.studentsName_listView)
        val studentsNameListView = ArrayAdapter(this,R.layout.students_list_item,studentsNameList)
        listView.adapter = studentsNameListView
    }


}